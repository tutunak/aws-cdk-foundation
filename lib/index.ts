import * as cdk from '@aws-cdk/core';

export interface AwsCdkFoundationProps {
  // Define construct properties here
}

export class AwsCdkFoundation extends cdk.Construct {

  constructor(scope: cdk.Construct, id: string, props: AwsCdkFoundationProps = {}) {
    super(scope, id);

    // Define construct contents here
  }
}
